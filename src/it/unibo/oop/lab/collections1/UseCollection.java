package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

    private static final int ELEMS = 1000000;
    private static final int TO_MS = 1000000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	
    	final List <Integer> arr = new ArrayList<>();
    	for(int i=1000; i<2000; i++) {
    		arr.add(i);
    	}
        
    	/*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	final List <Integer> arr2 = new LinkedList<>(arr);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	
		final Integer temp = arr.get(arr.size() - 1);
		arr.set(arr.size() - 1, arr.get(0));
		arr.set(0, temp);
    	    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	
    	for(Integer elem : arr) {
    		System.out.println(elem + " ");
    	}
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        long time = System.nanoTime();
        
        for(int i = 0; i<100000; i++) {
        	arr.add(0, i);
        }
        
        time = System.nanoTime() - time;
        System.out.println("Converting " + ELEMS
                + " int to String and inserting them in a ArrayList took " + time
                + "ns (" + time / TO_MS + "ms)");
        
        time = System.nanoTime();
        
        for(int i = 0; i<100000; i++) {
        	arr2.add(0, i);
        }
        
        time = System.nanoTime() - time;
        System.out.println("Converting " + ELEMS
                + " int to String and inserting them in a LinkedList took " + time
                + "ns (" + time / TO_MS + "ms)");

        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */

        time = System.nanoTime();

		for (int i = 0; i < 1000; i++) {
			arr.get(arr.size() / 2);
		}

        time = System.nanoTime() - time;
        System.out.println("Reading 1000 int from an ArrayList took " + time
                + "ns (" + time / TO_MS + "ms)");

        time = System.nanoTime();
 
		for (int i = 0; i < 1000; i++) {
			arr2.get(arr2.size() / 2);
		}

        time = System.nanoTime() - time;
        System.out.println("Reading 1000 int from a LinkedList took " + time
                + "ns (" + time / TO_MS + "ms)");

        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */

        Map<String, Long> myMap = new HashMap<>();
        myMap.put("Africa", 1110635000L);
        myMap.put("Americas", 972005000L);
        myMap.put("Antartica", 0L);
        myMap.put("Asia", 4298723000L);
        myMap.put("Europe", 742452000L);
        myMap.put("Oceania", 38304000L);
        
        /*
         * 
         * 8) Compute the population of the world
         */

        Long sum=0L;
        for(Long elem : myMap.values()) {
        	sum += elem;
        }
        System.out.println("Sum is:" + sum);


    }
}
