package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends RuntimeException {

	public NotEnoughBatteryException() {
		super();
	}

	public NotEnoughBatteryException(String message) {
		super(message);
	}

	public NotEnoughBatteryException(Throwable cause) {
		super(cause);
	}

	public NotEnoughBatteryException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotEnoughBatteryException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
